#! /usr/bin/env python3
# pylint: disable=missing-docstring

import argparse

import requests


# Global constants

METEO_URL = "https://www.metaweather.com/api/location"

# Helper constants

WEATHER_FIELD = 'consolidated_weather'
WEATHER_STATE_FIELD = 'weather_state_name'
DATE_FIELD = 'applicable_date'
MIN_TEMP_FIELD = 'min_temp'
MAX_TEMP_FIELD = 'max_temp'

HAPPY_FACE = ":-)"
SAD_FACE = ":-("

RAIN_WORD = ['storm', 'rain', 'shower']
MAX_DAYS = 7

SPACES = ' ' * 4


#
# Specific exceptions
#

class WeatherException(Exception):
    pass

class WeatherMultipleLocations(WeatherException):
    pass

class WeatherNoLocationFound(WeatherException):
    pass

class WeatherCorruptResponseData(WeatherException):
    pass

class WeatherUnknownError(WeatherException):
    pass

#
# Helper functions
#

def get_field(data, field):
    """Check if data structure has field <field>"""
    if not field in data.keys():
        raise WeatherCorruptResponseData("Missing '{}' field in data '{}'".format(field, data))
    return data[field]


def get_location(city):
    """Retrieve woeid if a valid location name was shipped"""
    resp = requests.get(METEO_URL + "/search", params={'query': city})
    locations = resp.json()

    if resp.status_code == 404 or not locations:
        raise WeatherNoLocationFound("No location found for city '{}'".format(city))

    if len(locations) > 1:
        raise WeatherMultipleLocations("Too many locations found for city '{}': {}"
                                       .format(city, locations))

    if 'woeid' in locations[0].keys():
        return locations[0]['woeid']
    raise WeatherCorruptResponseData("No 'woeid' field for city '{}' (retrieved data: {})"
                                     .format(city, locations))


def get_weather_for_location(woeid, days=None, pr_val=None, verbose=None):
    """Retrieving weather info for location identified woeid"""

    resp = requests.get("{}/{}/".format(METEO_URL, woeid))

    if resp.status_code == 404:
        raise WeatherNoLocationFound("No data returned for id {}".format(woeid))
    elif resp.status_code != 200:
        raise WeatherUnknownError("Unknown error occured: '{}'".format(resp.text))

    weather_data = resp.json()
    weather_fivedays = get_field(weather_data, WEATHER_FIELD)
    until = days if days else 0

    func = None
    if pr_val:
        func = weather_on_day
    elif verbose:
        func = details_of_day

    will_rain = False
    weather_details = []
    for day_data in weather_fivedays[0:until]:
        will_rain |= will_rain_on_day(day_data)
        if func:
            weather_details.append(func(day_data))
    return will_rain, weather_details


def will_rain_on_day(day_data):
    """Will rain on a particular day? (Given the weather data for the day"""
    return any(rain_word in weather_on_day(day_data).lower() for rain_word in RAIN_WORD)


def weather_on_day(day_data):
    """Weather info for the day"""
    return get_field(day_data, WEATHER_STATE_FIELD)


def details_of_day(day_data):
    """Get canonical date for the day"""
    fields = [DATE_FIELD, WEATHER_STATE_FIELD, MIN_TEMP_FIELD, MAX_TEMP_FIELD]
    return tuple([get_field(day_data, field) for field in fields])


def bool_to_resp(true_or_false):
    """Small function to turn a boolean to a response string"""
    return "Yes" if true_or_false else "No"


# pylint: disable=expression-not-assigned
def print_weather(will_rain, weather_details):
    """Print output most suitable to input structure"""
    if not weather_details:
        print(bool_to_resp(will_rain))
    elif not isinstance(weather_details[0], tuple):
        print(bool_to_resp(will_rain), weather_details)
    else:
        smiley = SAD_FACE if will_rain else HAPPY_FACE
        negate = 'BE' if will_rain else 'NOT be'
        print("   *** There will {} rain in the next {} day(s) {} ***"
              .format(negate, len(weather_details), smiley))
        [print("{}: {}, ({:.1f}/{:.1f} C)".format(*day_output))
         for day_output in weather_details if len(day_output) == 4]


#
# Main functionality
#

def main():
    """Main execution"""

    parser = argparse.ArgumentParser(description='Command-line tool to tell if it will rain '
                                                 'or not in the city in question')
    parser.add_argument('city', type=str, help='City in question')
    parser.add_argument('days', metavar='N', type=int, nargs='?', default=1,
                        help='number of days to take into account (max: {})'.format(MAX_DAYS))

    parser.add_argument('-p', '--print', dest='pr', action='store_true',
                        help='Print the exact weather forecast string(s)')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                        help='Print the date and the weather forecast string with it')

    args = parser.parse_args()

    woeid = get_location(args.city)
    will_rain, weather_details = get_weather_for_location(woeid, args.days, args.pr, args.verbose)
    print_weather(will_rain, weather_details)
    return will_rain


#
# Execution as a script
#

if __name__ == '__main__':
    try:
        main()
    except WeatherException as err:
        print("[ERROR]: {}".format(err))

# Will it rain?

Small command-line tool to provide prompt weather forecast -- especially on expected rain status.

## Usage

...is best explained by the tool's help:

```
usage: will_rain [-h] [-p] [-v] city [N]

Command-line tool to tell if it will rain or not in the city in question

positional arguments:
  city           City in question
  N              number of days to take into account (max: 7)

optional arguments:
  -h, --help     show this help message and exit
  -p, --print    Print the exact weather forecast string(s)
  -v, --verbose  Print the date and the weather forecast string with it
```

And this is what it looks like, practically applied:

 No rain in the next 3 days!!!

```bash
$ will_rain.py paris 3
No
$ will_rain.py paris 3 -p
No ['Clear', 'Clear', 'Light Cloud']
$ will_rain.py paris 3 -v
   *** There will NOT be rain in the next 3 day(s) :-) ***
2018-09-26: Clear, (5.4/20.7 C)
2018-09-27: Clear, (9.7/24.9 C)
2018-09-28: Light Cloud, (10.6/22.3 C)
```

 ...but after there will be some...

```bash
$ will_rain.py paris 5
Yes
$ will_rain.py paris 5 -p
Yes ['Clear', 'Clear', 'Light Cloud', 'Light Cloud', 'Light Rain']
$ will_rain.py paris 5 -v
   *** There will BE rain in the next 5 day(s) :-) ***
2018-09-26: Clear, (5.4/20.7 C)
2018-09-27: Clear, (9.7/24.9 C)
2018-09-28: Light Cloud, (10.6/22.3 C)
2018-09-29: Light Cloud, (7.6/17.2 C)
2018-09-30: Light Rain, (6.2/17.2 C)
```

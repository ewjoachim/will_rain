"""Setup for the 'will_rain' command-line tool"""
import os
from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


with open('requirements.txt') as f:
    REQUIREMENTS = f.read().splitlines()

setup(
    name='will_rain',
    version='0.0.1',
    install_requires=REQUIREMENTS,
    description='Small weather forecast command-line tool',
    long_description=README,
    url='https://gitlab.com/judit.novak/will_rain/',
    author='Judit Novak',
    author_email='judit.novak@gmail.com',
    scripts=['will_rain'],
)
